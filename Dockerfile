FROM jboss/wildfly

ADD target/crud-rest-wildfly.war /opt/jboss/wildfly/standalone/deployments/

# ENTRYPOINT ["java", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:8000","-jar","app.jar"]