package org.jboss.as.quickstarts.rest;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.jboss.as.quickstarts.model.Produto;
import org.jboss.as.quickstarts.model.ProdutoDAO;

import jakarta.inject.Named;

@Stateless
@Path("/produto")
@Named(value = "clienteBean")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProdutoRest {

    @EJB
    ProdutoDAO produtoDAO;

    @GET
    public Response listagem() {
        return Response.ok(produtoDAO.findAll()).build();
    }

    @GET
    @Path("/{id}")
    public Response detalhe(@PathParam("id") Long id) throws Exception {
        ResponseBuilder response = null;

        try {
            response = Response.ok(produtoDAO.findById(id));
        } catch (Exception exc) {
            response = Response.status(Status.NOT_FOUND);
        }

        return response.build();
    }

    @POST
    public Response salvar(Produto produto) {
        ResponseBuilder response = null;

        try {
            response = Response.ok(produtoDAO.salvar(produto));
        } catch (Exception e) {
            response = Response.status(Status.INTERNAL_SERVER_ERROR);
        }

        return response.build();
    }

    @PUT
    @Path("/{id}")
    public Response salvar(@PathParam("id") Long id, Produto produto) {
        ResponseBuilder response = null;

        try {
            produtoDAO.findById(id);
            produto.setId(id);
            response = Response.ok(produtoDAO.salvar(produto));
        } catch (Exception e) {
            response = Response.status(Status.INTERNAL_SERVER_ERROR);
        }

        return response.build();
    }

    @DELETE
    @Path("/{id}")
    public Response excluir(@PathParam("id") Long id) throws Exception {
        ResponseBuilder response = null;
        try {
            produtoDAO.delete(id);
            response = Response.ok();
        } catch (Exception e) {
            response = Response.status(Status.NOT_FOUND);
        }

        return response.build();
    }

}
