package org.jboss.as.quickstarts.model;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import jakarta.transaction.TransactionScoped;

@Stateless
public class ProdutoDAO {

    @PersistenceContext(unitName = "primary")
    private EntityManager entityManager;

    public List<Produto> findAll() {
        return entityManager.createNamedQuery("Produto.findAll", Produto.class).getResultList();
    }

    public Produto findById(Long id) throws Exception {
        Produto produto = entityManager.find(Produto.class, id);

        if (produto == null) {
            throw new Exception("Registro não encontrado ID(".concat(id.toString(0).concat(")")));
        }

        return produto;
    }

    @TransactionScoped
    public Produto salvar(Produto produto) throws Exception {
        if (produto.getId() != null) {
            produto = this.findById(produto.getId());
        }
        entityManager.persist(produto);

        return produto;
    }

    public void delete(Long id) throws Exception {
        Produto produto = this.findById(id);

        entityManager.remove(produto);
    }
}
