package org.jboss.as.quickstarts.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;


import lombok.Data;

@Data
@Entity
@Table(name = "tb_produto")
@NamedQueries(@NamedQuery(name = "Produto.findAll", query = "Select p from Produto p ORDER BY p.id"))
public class Produto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(length = 50, nullable = false)
    private String nome;

    @NotNull
    @Column(length = 150)
    private String descricao;
}